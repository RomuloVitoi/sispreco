# Sispreco

## Funcionalidades

### Frontend
- 🚫 Realizar login
- 🚫 Importar dados do IBGE
- 🚫 Realizar levantamento de preço
- 🚫 Gerar relatórios
- 🚫 Definir quais produtos serão utilizados no levantamento em um município
- 🚫 Definir quais usuários podem realizar o levantamento em um município
- 🚫 Definir preço mínimo e máximo para um produto

### Backend
- 🚫 Validar login
- ✅️ Importar dados do IBGE - Tabela PEVS
- ✅ Importar dados do IBGE - Tabela PPM
- ✅ Importar dados do IBGE - Tabbela LSPA
- ✅ Realizar levantamento de preço
- ✅ Definir quais produtos serão utilizados no levantamento de preço em um município.
- ✅ Definir quais usuários podem realizar o levantamento em um município
- ✅ Gerar relatório de preços semanal por produto e por município
- 🚫 Gerar relatório de preços semanal por produto
- 🚫 Gerar relatório de séries históricas
- 🚫 Definir preço mínimo e máximo para um produto
- 🚫 Validar preço do levantamento baseado nos preços mínmos e máximos


## Rotas da API
### Incaper
- `GET /municipios`

  Retorna a lista de muncípios.

- `GET /municipio/{id}`

  Retorna os dados de um município.

- `GET /beneficiarios`

  Retorna a lista de beneficiários.

- `GET /beneficiarios/municipio/{id}`

  Retorna a lista de beneficiários de um município.

- `GET /beneficiario/{id}`

  Retorna os dados de um beneficiário.
  
- `GET /produtos`

  Retorna a lista de produtos.

- `GET /produto/{id}`

  Retorna os dados de um produto.


### Sispreço

- `GET /produtos`
  
  Retorna a lista de produtos.

- `GET /produtores/municipio/{id}`
  
  Retorna a lista de produtores de um município.

- `POST /levantamento`
  
  Realiza um levantamento de preço.
  
  **Parâmetros:**
  - `produto` - código do produto
  - `produtor` - código do produtor
  - `municipio` - código do município
  - `preco` - preço

- `GET /levantamento/produtos`

  Retorna a lista de produtos para se realizar o levantamento em um município.

  **Parâmetros:**
  - `municipio` - código do município
  - `ano` - integer - ano do levantamento
  
- `POST /levantamentos/produtos`

  Atualiza a lista de produtos para se realizar o levantamento em um município.

  **Parâmetros (JSON body):**
  - `municipio` - código do município
  - `ano` - ano do levantamento
  - `produtos` - lista de códigos de produtos

- `GET /levantamento/agentes`

  Retorna a lista de agentes que podem realizar o levantamento em um município.

  **Parâmetros:**
  - `municipio` - código do município
  
- `POST /levantamentos/agentes`

  Atualiza a lista de agentes que podem realizar o levantamento em um município.

  **Parâmetros (JSON body):**
  - `municipio` - código do município
  - `agentes` - lista de identificadores de agentes

- `POST /importar/pevs`

  Importa os dados do IBGE - Tabela PEVS.

  **Parâmetros:**
  - `csv` - arquivo csv para ser importado
  - `force` - ignora os erros e força a importação se presente (opcional)

- `POST /importar/ppm`

  **Parâmetros:**
  - `csv` - arquivo csv para ser importado
  - `force` - ignora os erros e força a importação se presente (opcional) 
  
  Importa os dados do IBGE - Tabela PPM.
  
- `POST /importar/lspa`
  
  Importa os dados do IBGE - Tabela LSPA.
  
  **Parâmetros:**
  - `csv` - arquivo csv para ser importado
  - `force` - ignora os erros e força a importação se presente (opcional)

- `POST /relatorio/gerar`
  