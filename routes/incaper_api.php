<?php

/*
|--------------------------------------------------------------------------
| Incaper API Routes
|--------------------------------------------------------------------------
*/

Route::get('/municipios', 'MunicipioController@all');
Route::get('/municipio/{id}', 'MunicipioController@show');

Route::get('/beneficiarios', 'BeneficiarioController@all');
Route::get('/beneficiarios/municipio/{id}', 'BeneficiarioController@municipio');
Route::get('/beneficiario/{id}', 'BeneficiarioController@show');

Route::get('/produtos', 'ProdutoController@all');
Route::get('/produto/{id}', 'ProdutoController@show');
