ALTER TABLE produtos_agricultura RENAME TO produtos;
ALTER TABLE produtos CHANGE id_produto_agricultura id_produto INT(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE produtos CHANGE nm_produto_agricultura nm_produto VARCHAR(200) NOT NULL;
ALTER TABLE produtos MODIFY nm_codigo_ibge VARCHAR(100);
