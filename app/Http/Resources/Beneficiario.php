<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Beneficiario extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id_beneficiario'],
            'nome' => $this['nm_nome'],
            'apelido' => $this['nm_apelido'],
            'telefone' => $this['nm_telefone']
        ];
    }
}
