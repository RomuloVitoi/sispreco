<?php

namespace App\Http\Controllers;

use App\Http\Resources\Municipio as MunicipioResource;
use App\Models\Municipio;
use App\Models\UnidadeFederativa;

class MunicipioController extends Controller
{
    public function all()
    {
        return MunicipioResource::collection(
            Municipio::where(
                'id_unidade_federativa_FK',
                UnidadeFederativa::where('nm_sigla', 'ES')->first()['id_unidade_federativa']
            )->get());
    }

    public function show($id)
    {
        return MunicipioResource::make(Municipio::find($id)->first());
    }

    public function get()
    {
        return [
            'data' => Municipio::getAll(),
        ];
    }
}
