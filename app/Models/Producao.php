<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Producao
 *
 * @property int $id
 * @property string $produto
 * @property int $municipio
 * @property int $quantidade
 * @property int $ano
 * @property int $mes
 * @property string $tipo
 *
 * @package App\Models
 */
class Producao extends Model
{
    protected $table = 'producao';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'produto',
        'municipio',
        'quantidade',
        'ano',
        'mes',
        'tipo',
    ];
}
