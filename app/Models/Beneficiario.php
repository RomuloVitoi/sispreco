<?php

namespace App\Models;

use App\Facades\Incaper;
use Illuminate\Support\Facades\Cache;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Beneficiario
 *
 * @property int $id_beneficiario
 * @property int $id_municipio_FK
 * @property int $id_beneficiario_FK
 * @property string $nm_nome
 * @property string $nm_apelido
 * @property string $nm_endereco
 * @property string $nm_telefone
 * @property string $nm_email
 * @property string $nm_contato
 * @property string $nm_referencia_localizacao
 *
 * @property \App\Models\Municipio $municipio
 *
 * @package App\Models
 */
class Beneficiario extends Eloquent
{
    protected $connection = 'incaper';
    protected $table = 'beneficiarios';
    protected $primaryKey = 'id_beneficiario';
    public $timestamps = false;

    protected $casts = [
        'id_municipio_FK'         => 'int',
        'id_beneficiario_FK'      => 'int',
        'id_comunidade_FK'        => 'int',
        'id_estado_civil_FK'      => 'int',
        'id_grau_escolaridade_FK' => 'int',
        'id_tipo_publico_FK'      => 'int',
        'id_sexo_FK'              => 'int',
        'nr_residentes'           => 'int',
        'nr_renda_familiar'       => 'float',
    ];

    protected $dates = [
        'dt_data_nascimento',
    ];

    protected $fillable = [
        'id_municipio_FK',
        'id_beneficiario_FK',
        'nm_nome',
        'nm_apelido',
        'nm_endereco',
        'nm_telefone',
        'nm_email',
        'nm_contato',
        'nm_referencia_localizacao',
    ];

    public function municipio()
    {
        return $this->belongsTo(\App\Models\Municipio::class, 'id_municipio_FK');
    }

    /**
     * Retorna os beneficiários da API do Incaper.
     *
     * @param $id
     *
     * @return array
     */
    public static function getFromIncaper($id)
    {
        return Cache::remember("produtores_$id", 10, function () use ($id) {
            $response = Incaper::get("/incaper/api/beneficiarios/municipio/$id");

            return array_get(json_decode($response->getBody(), true), 'data', []);
        });
    }
}
