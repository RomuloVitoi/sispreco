<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLevantamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('levantamentos', function (Blueprint $table) {
            $table->string('autor');
            $table->string('produto')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levantamentos', function (Blueprint $table) {
            $table->removeColumn('autor');
            $table->integer('produto')->change();
        });
    }
}
